%%TEST_STRATA_DIMS()
%
%Check that infection model parameters and model of care parameters are in
%agreement with the number of population strata.

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [success] = test_strata_dims()
    params = model_params();
    moc = model_of_care('default', 'moderate');

    strata = params.pi;
    strata_count = length(strata);
    strata_size = size(strata);
    square_size = [strata_count strata_count];
    success = true();

    %% The model parameters whose size should match the population strata.
    same_size = {'delta', 'sigma_1', 'sigma_2', ...
                 'gamma_1', 'gamma_2', 'gamma_1q', 'gamma_2q', ...
                 'eta', 'eta_relscale', 'propn_icu'};
    %% The model parameters that should be square matrices whose size should
    %% match the population strata.
    square_mat = {'kappa', 'tpm'};

    %% The MOC parameters whose size should match the population strata.
    moc_same_size = {'ward_to_ICU', 'ICU_to_death', 'noICU_to_death'};

    for i = 1:length(same_size)
        field_name = same_size{i};
        if ~ isfield(params, field_name)
            success = false();
            fprintf('Missing parameter "%s"\n', field_name);
        end
        actual_size = size(getfield(params, field_name));
        if ~ isequal(actual_size, strata_size)
            success = false();
            fprintf('Parameter "%s" has incorrect size', field_name);
        end
    end

    for i = 1:length(square_mat)
        field_name = square_mat{i};
        if ~ isfield(params, field_name)
            success = false();
            fprintf('Missing parameter "%s"', field_name);
        end
        actual_size = size(getfield(params, field_name));
        if ~ isequal(actual_size, square_size)
            success = false();
            fprintf('Parameter "%s" has incorrect size', field_name);
        end
    end

    for i = 1:length(moc_same_size)
        field_name = moc_same_size{i};
        if ~ isfield(moc, field_name)
            success = false();
            fprintf('Missing MOC parameter "%s"\n', field_name);
        end
        actual_size = size(getfield(moc, field_name));
        if ~ isequal(actual_size, strata_size)
            success = false();
            fprintf('MOC parameter "%s" has incorrect size', field_name);
        end
    end
end

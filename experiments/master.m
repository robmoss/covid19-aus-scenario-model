function [model, param_dists, samples, param1, curr_dir] = master(R0, severity, pm, qeff, meff, rho, lhs_samples)
    %MASTER  The master file that defines parameters and runs LHS experiments.
    %
    % [mat_file] = master(R0, severity, pre_symp, pm, qeff, meff, rho, lhs_samples)
    %
    % Inputs:
    %   R0            - The value of R0.
    %   severity      - Disease severity (1..4, see below).
    %   pm            - Proportion of presenting cases that are isolated.
    %   qeff          - Reduction in infectiousness for quarantined contacts.
    %   meff          - Reduction in infectiousness for isolated cases (I2).
    %   rho           - Proportion of identified contacts that self-quarantine.
    %   lhs_samples   - The (optional) number of LHS samples to simulate.
    %                   Default: 10,000 samples (only override when necessary).
    %
    % Output:
    %   mat_file - The name of the simulation output file.
    %
    % Severity:
    %   -------------
    %   1    Low
    %   2    Medium
    %   3    High
    %   4    High, with age-specific breakdown
    %

    % Copyright 2020 Robert Moss.
    % SPDX-License-Identifier: GPL-3.0-or-later

    curr_dir = pwd();
    exp_dir = fileparts(mfilename('fullpath'));
    model_dir = fileparts(exp_dir);

    %% Change directory to the one containing the model code.
    cd(model_dir);
    setpaths();

    %% Load the model definition.
    model = discrete_model();

    %% Define the transmission parameter (R0).
    param_dists.R0 = dist_const(R0);

    %% The incubation period is 3.2 days (1.6 in each of E1 and E2);
    %% The infectious period is 9.68 days (4 in I1 and 5.68 in I2); and
    %% The doubling time is 6.4 days.

    %% Retrieve the number of population strata.
    temp_params = model.params();
    n = length(temp_params.pi);
    %% The mean duration of residence for E1 and for E2 (1.6 days).
    sigma = 365 / 1.6 * ones(n, 1);
    %% The mean duration of residence for I1 (4 days)
    gamma_1 = 365 / 4 * ones(n, 1);
    %% The mean duration of residence for I2 (5.68 days)
    gamma_2 = 365 / 5.68 * ones(n, 1);

    %% Change the rate parameters for each E and I compartment.
    param_dists.sigma_1 = dist_const(sigma);
    param_dists.sigma_2 = dist_const(sigma);
    param_dists.gamma_1 = dist_const(gamma_1);
    param_dists.gamma_2 = dist_const(gamma_2);
    %% NOTE: in this scenario, case isolation doesn't occur any quicker
    %% for contacts who are adhering to self-quarantine.
    param_dists.gamma_1q = dist_const(gamma_1);
    param_dists.gamma_2q = dist_const(gamma_2);

    %% NOTE: ascertainment and isolation of cases.
    param_dists.PrAscertain = dist_const(pm);
    param_dists.ImpactOfAscertain = dist_const(meff);

    %% NOTE: self-quarantine of identified contacts.
    param_dists.ImpactOfQuarantine = dist_const(qeff);
    param_dists.rho = dist_const(rho);

    %% Define the severity parameter (eta).
    if any(severity == [1])
        param_dists.eta = dist_logflat(-4, -3);
    elseif any(severity == [2])
        param_dists.eta = dist_logflat(-3, -2);
    elseif any(severity == [3])
        param_dists.eta = dist_logflat(-2, -1);
    elseif any(severity == [4])
        %% NOTE: a base rate of 5-10%, which means 50-100% for those aged 80+.
        param_dists.eta = dist_logflat(-1.3, -1);
    else
        throw(MException('Master Experiment', 'Invalid severity'))
    end

    %% The number of LHS samples.
    if exist('lhs_samples', 'var')
        samples = lhs_samples;
    else
        samples = 1e4;
    end

    param1 = struct(...
        'name', 'dummy', ...
        'description', 'dummy', ...
        'values', 1);

    %% Note: each scenario file must call LHS() directly, so that the output
    %%       file name will match that of the scenario file.
    %% [mat_file] = lhs(model, param_dists, samples, param1);

    %% Restore the previous working directory.
    %% cd(curr_dir);
end

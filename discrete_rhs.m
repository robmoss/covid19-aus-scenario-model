%% Model Equations
%%
%% Calculates the change in the state of the model.

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [state_delta] = discrete_rhs(time, state, params)
    %%
    %% Events that occur during the epidemic (ie, the beginning and the end of
    %% the Contain phase, and the complete depletion of the stockpile) are
    %% recorded in the global record |events|.
    %%
    global events;

    %%
    %% Scale time-dependent parameters to be per time-step
    %%
    params.kappa = params.kappa / events.timescale;
    params.delta = params.delta / events.timescale;
    params.sigma_1 = params.sigma_1 / events.timescale;
    params.sigma_2 = params.sigma_2 / events.timescale;
    params.gamma_1 = params.gamma_1 / events.timescale;
    params.gamma_2 = params.gamma_2 / events.timescale;
    params.gamma_1q = params.gamma_1q / events.timescale;
    params.gamma_2q = params.gamma_2q / events.timescale;
    params.LambdaImport = params.LambdaImport / events.timescale;

    %% Round up if we've crossed into negative values
    state(state < 0) = 0;

    %% Fix shape of params.alpha_m (dep distribution limitation)
    params.alpha_m = params.alpha_m';

    %%
    %% The model state is stored as a vector; each state variable is extracted
    %% from the vector and associated with a meaningful name.
    %%
    %% state = [S; E1; E2; I1; I2; R; M; RM; CT_m; CT_nm;
    %%          E1q; E2q; I1q; I2q; Rq; Mq; RMq; ];
    n = length(params.pi);
    S = state(1:n);
    E1 = state(1*n+1:2*n);
    E2 = state(2*n+1:3*n);
    I1 = state(3*n+1:4*n);
    I2 = state(4*n+1:5*n);
    R = state(5*n+1:6*n);
    M = state(6*n+1:7*n);
    RM = state(7*n+1:8*n);
    CT_m = state(8*n+1:9*n);
    CT_nm = state(9*n+1:10*n);
    E1q = state(10*n+1:11*n);
    E2q = state(11*n+1:12*n);
    I1q = state(12*n+1:13*n);
    I2q = state(13*n+1:14*n);
    Rq = state(14*n+1:15*n);
    Mq = state(15*n+1:16*n);
    RMq = state(16*n+1:17*n);

    %% Population sizes for convenience
    Ns = params.pi .* params.N;

    %% Presentation rates derived from parameters $\eta$ and $\alpha_M$.
    if isfield(params, 'eta_relscale')
        %% Important: non-uniform values for 'eta_relscale' will change the
        %% overall proportion of infections in each strata that present, since
        %% 'alpha_m' is the proportion of *remaining* (i.e., non-severe)
        %% infections that present.
        params.eta = params.eta .* params.eta_relscale;
    end
    alpha = params.eta + params.alpha_m .* (1  - params.eta);

    %% Calculate stochastic flow rates between compartments.
    function rate = stoch_flow(People, Pr)
        %% Initialise output to ensure shape (size(People)) is maintained.
        rate = zeros(size(People));
        People = floor(People);

        %% When the expected (yearly) rate is greater than the number of
        %% people, stochastically choose the number of people to move in a
        %% single timestep and then scale this to get the yearly rate.

        %% Set rates to zero when there are no people or zero probability
        zero_inds = or( People < 1, Pr == 0 );
        %% zero_inds = or( People == 0, Pr == 0 );
        rate(zero_inds) = 0;

        if any(Pr > 1)
          fprintf(1, 'ERROR: Pr > 1\n');
        end

        %% Use the Poisson distribution (the law of rare events)
        %% when People > 1e3 and Pr < 1e-4 (ie, B(n,p) ~ P(np))
        %% NOTE: delT must be smaller than all rate constants
        poiss_inds = and( People >= 1e3, ~zero_inds);
        if nnz(poiss_inds) > 0
            rate(poiss_inds) = poissrnd(People(poiss_inds) .* Pr(poiss_inds));
        end

        %% Use binomial selection in all other cases
        bino_inds = and(~zero_inds, ~poiss_inds);
        if nnz(bino_inds) > 0
            rate(bino_inds) = binornd(People(bino_inds), Pr(bino_inds));
        end
    end

    %% Calculate the force of infection exerted by each strata.
    beta = (params.R0scaling .* params.kappa)';
    beta_I1 = beta;
    beta_I2 = beta;
    beta_M = beta_I2 .* (1 - params.ImpactOfAscertain);
    %% NOTE: individuals in the MQ compartment, who were quarantined and then
    %% presented and were ascertained, should have their infectiousness
    %% reduced by whichever action (quarantine or isolation) has the greatest
    %% effect.
    beta_Mq = beta_I2 * (1 - max(params.ImpactOfQuarantine, ...
                                 params.ImpactOfAscertain));

    import = params.LambdaImport * params.pi;
    lambda = (import ...
             + beta_I1 * I1 ...
             + beta_I2 * I2 ...
             + beta_M * M ...
             + beta_I1 * I1q .* (1 - params.ImpactOfQuarantine) ...
             + beta_I2 * I2q .* (1 - params.ImpactOfQuarantine) ...
             + beta_Mq * Mq);

    %%
    %% Calculate the expected values of the stochastic flows.
    %%
    People = [S E1 E2 I1 I2 M E1q E2q I1q I2q Mq];
    Probs = [lambda ./ Ns...
             params.sigma_1 params.sigma_2 ...
             params.gamma_1 params.gamma_2 ...
             params.gamma_2 ...
             params.sigma_1 params.sigma_2 ...
             params.gamma_1q params.gamma_2q ...
             params.gamma_2q];

    %%
    %% Calculate the stochastic flows.
    %%
    stoch_rates = stoch_flow(People, Probs);
    %% NOTE: can also use the deterministic flows for now ...
    %% stoch_rates = People .* Probs;
    out_S = stoch_rates(:, 1);
    out_E1 = stoch_rates(:, 2);
    out_E2 = stoch_rates(:, 3);
    out_I1 = stoch_rates(:, 4);
    out_I2 = stoch_rates(:, 5);
    out_M = stoch_rates(:, 6);
    out_E1q = stoch_rates(:, 7);
    out_E2q = stoch_rates(:, 8);
    out_I1q = stoch_rates(:, 9);
    out_I2q = stoch_rates(:, 10);
    out_Mq = stoch_rates(:, 11);

    %%
    %% Account for case ascertainment (I1 -> M).
    %%
    People = out_I1;
    %% NOTE: account for case visibility due to presenting (alpha).
    Probs = params.PrAscertain * ones(n, 1) .* alpha;
    out_I1_to_M = stoch_flow(People, Probs);
    out_I1_to_I2 = out_I1 - out_I1_to_M;
    %% Account for ascertainment of quarantined contacts.
    People = out_I1q;
    out_I1q_to_Mq = stoch_flow(People, Probs);
    out_I1q_to_I2q = out_I1q - out_I1q_to_Mq;

    %% Calculate flows for the two types of contacts:
    %% - Those who are contacts of cases that were ascertained and are being
    %%   managed (CT_m);
    %% - Those who are contacts of cases that are not being managed (CT_nm)).
    CT_m_in = params.kappa' * (out_I1_to_M + out_I1q_to_Mq);
    CT_nm_in = params.kappa' * (out_I1_to_I2 + out_I1q_to_I2q);
    Theta_denom = CT_m + CT_nm;
    denom_z_mask = Theta_denom == 0;
    Theta_m = (S ./ Ns) .* CT_m ./ (CT_m + CT_nm);
    Theta_nm = (S ./ Ns) .* CT_nm ./ (CT_m + CT_nm);
    Theta_m(denom_z_mask) = 0;
    Theta_nm(denom_z_mask) = 0;
    CT_m_out = params.delta .* CT_m + lambda .* Theta_m;
    CT_nm_out = params.delta .* CT_nm + lambda .* Theta_nm;

    %% NOTE: distribute out_S to E1 and E1q in proportion to
    %% Theta_nm (E1) and Theta_m (E1q).
    %% A proportion (rho) of a managed case's contacts will self-quarantine.
    %% All other exposed individuals will not self-quarantine.
    Prob = params.rho .* Theta_m ./ (Theta_m + Theta_nm);
    denom = Theta_m + Theta_nm;
    Prob(denom == 0) = 0;
    out_S_E1q = stoch_flow(out_S, Prob);
    out_S_E1 = out_S - out_S_E1q;

    %%
    %% Return the change to model state.
    %%
    dS = - out_S;
    dE1 = out_S_E1 - out_E1;
    dE2 = out_E1 - out_E2;
    dI1 = out_E2 - out_I1;
    dI2 = out_I1_to_I2 - out_I2;
    dM = out_I1_to_M - out_M;
    dR = out_I2;
    dRM = out_M;
    dCT_m = CT_m_in - CT_m_out;
    dCT_nm = CT_nm_in - CT_nm_out;
    dE1q = out_S_E1q - out_E1q;
    dE2q = out_E1q - out_E2q;
    dI1q = out_E2q - out_I1q;
    dI2q = out_I1q_to_I2q - out_I2q;
    dMq = out_I1q_to_Mq - out_Mq;
    dRq = out_I2q;
    dRMq = out_Mq;

    state_delta = [dS; dE1; dE2; dI1; dI2; dR; dM; dRM; dCT_m; dCT_nm; ...
                   dE1q; dE2q; dI1q; dI2q; dRq; dMq; dRMq; ];

    %% Sanity checks
    if (length(state_delta) ~= length(state))
        length(state)
        length(state_delta)
        fprintf(1,' ERROR: State vector length not conserved\n');
    end
    if any(or(isnan(state_delta), isinf(state_delta)))
        fprintf(1, 'ERROR: NaN or Inf at t = %d\n', time);
    end
    if any(state < 0)
        i = find(state < 0, 1);
        fprintf(1, 'ERROR: Negative state (%d = %f) at t = %d\n', ...
                i, state(i), time);
    end
end

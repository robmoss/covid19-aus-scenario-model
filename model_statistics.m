% MODEL_STATISTICS(time, params, state, events)
%
% Analyses the model output and returns various statistics that relate to the
% load on the healthcare system and the delivery of vaccinations.
%

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [output] = model_statistics(time, params, state, events)
    if nargin == 2
        n = length(params.pi);
        n_steps = floor(365 * params.time_end - params.time_start);
        n_vec = params.time_entries + 1;
        n_vec = n_steps + 1;
        output = struct(...
            'AttackRate', zeros(n, 1), ...
            'ClinicalAttackRate', zeros(n, 1), ...
            'CAR05', zeros(1, 1), ...
            'CAR50', zeros(1, 1), ...
            'CAR95', zeros(1, 1), ...
            'CAR96', zeros(1, 1), ...
            'CAR97', zeros(1, 1), ...
            'CAR98', zeros(1, 1), ...
            'CAR99', zeros(1, 1), ...
            'CAR100', zeros(1, 1), ...
            'EpidemicDuration', zeros(1,1), ...
            'DailyIncidence', zeros(n, n_steps + 1), ...
            'DailyPresentations', zeros(n, n_steps + 1), ...
            'DailyPresentationsMild', zeros(n, n_steps + 1), ...
            'DailyPresentationsSevere', zeros(n, n_steps + 1), ...
            'TimeOfPeak', zeros(n, 1), ...
            'PeakDailyPresentations', zeros(n, 1) ...
            );
        return;
    elseif nargin ~= 4
        fprintf(1, 'ERROR: invalid # of parameters to healthcare\n');
        return;
    end

    n = length(params.pi);

    n_vec = params.time_entries + 1;

    %% Extract relevant state variables
    n = length(params.pi);
    S = state(:, 1:n)';
    E1 = state(:, 1*n+1:2*n)';
    E2 = state(:, 2*n+1:3*n)';
    I1 = state(:, 3*n+1:4*n)';
    I2 = state(:, 4*n+1:5*n)';
    R = state(:, 5*n+1:6*n)';
    M = state(:, 6*n+1:7*n)';
    RM = state(:, 7*n+1:8*n)';
    CT_m = state(:, 8*n+1:9*n)';
    CT_nm = state(:, 9*n+1:10*n)';
    E1q = state(:, 10*n+1:11*n)';
    E2q = state(:, 11*n+1:12*n)';
    I1q = state(:, 12*n+1:13*n)';
    I2q = state(:, 13*n+1:14*n)';
    Rq = state(:, 14*n+1:15*n)';
    Mq = state(:, 15*n+1:16*n)';
    RMq = state(:, 16*n+1:17*n)';

    %% Population sizes for convenience
    Ns = params.pi .* params.N;

    %% Fix shape of params.alpha_m (dep distribution limitation)
    params.alpha_m = params.alpha_m';

    %% Presentation rates derived from parameters $\eta$ and $\alpha_M$.
    if isfield(params, 'eta_relscale')
        %% Important: non-uniform values for 'eta_relscale' will change the
        %% overall proportion of infections in each strata that present, since
        %% 'alpha_m' is the proportion of *remaining* (i.e., non-severe)
        %% infections that present.
        params.eta = params.eta .* params.eta_relscale;
    end
    alpha = params.eta + params.alpha_m .* (1  - params.eta);

    %% Calculate the attack rate and clinical attack rate.
    AllR = R(:, end) + RM(:, end) + Rq(:, end) + RMq(:, end);
    AttackRate = AllR ./ (params.N * params.pi);
    ClinicalAttackRate = AttackRate .* alpha;

    %% Calculate daily incidence and presentations.
    CumInc = Ns - S - E1 - E2 - E1q - E2q;
    DailyIncidence = events.timescale / 365 * [CumInc(:, 1), diff(CumInc, 1, 2)];
    DailyIncidence = DailyIncidence(:, 1:events.timescale / 365:end);
    DailyPresentations = DailyIncidence .* alpha;
    DailyPresentationsMild = DailyIncidence .* ...
                             params.alpha_m .* (1  - params.eta);
    DailyPresentationsSevere = DailyIncidence .* params.eta;

    %% Calculate the time and size of the presentation peak for each strata.
    [PeakDailyPresentations, index] = max(DailyPresentations, [], 2);
    TimeOfPeak = index;

    %% Identify when the CAR reached 5%, 50% and 95% of the final value.
    FinalPres = squeeze(R(:, end) + RM(:, end) + Rq(:, end) + RMq(:, end)) ...
                .* squeeze(alpha);
    FinalCAR = sum(FinalPres) / params.N;
    CumPres = (R + RM + Rq + RMq) .* squeeze(alpha);
    CumPres = squeeze(sum(CumPres, 1));
    CumCAR = CumPres / params.N;
    ix05 = find(CumCAR >= 0.05 * FinalCAR, 1);
    ix50 = find(CumCAR >= 0.50 * FinalCAR, 1);
    ix95 = find(CumCAR >= 0.95 * FinalCAR, 1);
    ix96 = find(CumCAR >= 0.96 * FinalCAR, 1);
    ix97 = find(CumCAR >= 0.97 * FinalCAR, 1);
    ix98 = find(CumCAR >= 0.98 * FinalCAR, 1);
    ix99 = find(CumCAR >= 0.99 * FinalCAR, 1);
    ix100 = find(CumCAR == FinalCAR, 1);
    tCAR_05 = (365/events.timescale) * time(ix05);
    tCAR_50 = (365/events.timescale) * time(ix50);
    tCAR_95 = (365/events.timescale) * time(ix95);
    tCAR_96 = (365/events.timescale) * time(ix96);
    tCAR_97 = (365/events.timescale) * time(ix97);
    tCAR_98 = (365/events.timescale) * time(ix98);
    tCAR_99 = (365/events.timescale) * time(ix99);
    tCAR_100 = (365/events.timescale) * time(ix100);
    duration = tCAR_95 - tCAR_05;

    %% Collect all of the statistics
    output.AttackRate = AttackRate;
    output.ClinicalAttackRate = ClinicalAttackRate;
    output.CAR05 = tCAR_05;
    output.CAR50 = tCAR_50;
    output.CAR95 = tCAR_95;
    output.CAR96 = tCAR_96;
    output.CAR97 = tCAR_97;
    output.CAR98 = tCAR_98;
    output.CAR99 = tCAR_99;
    output.CAR100 = tCAR_100;
    output.EpidemicDuration = duration;
    output.DailyIncidence = DailyIncidence;
    output.DailyPresentations = DailyPresentations;
    output.DailyPresentationsMild = DailyPresentationsMild;
    output.DailyPresentationsSevere = DailyPresentationsSevere;
    output.TimeOfPeak = TimeOfPeak;
    output.PeakDailyPresentations = PeakDailyPresentations;
end

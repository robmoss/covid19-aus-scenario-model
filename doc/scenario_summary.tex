\documentclass[DIV14]{scrartcl}

\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{float}
\usepackage{xcolor}
\usepackage{hyperref}

\usepackage[square,numbers]{natbib}

\title{Summary document detailing Australian modelling for COVID-19 scenarios:
  parameter values for epidemic scenarios}
\author{}
%% \date{}

\begin{document}

\begin{center}
  \large
  \textsf{\textbf{\textcolor{red}{%
        CONFIDENTIAL (not for onwards circulation)}}}
  \vspace{1.5ex}

  \makeatletter
  \Large \textsf{\textbf{\@title}}
  \vspace{1.5ex}

  \@date
  \makeatother
\end{center}

\section{Introduction}

These scenarios are based on the ``worst case'' pandemic influenza scenarios
described in the Australian Health Management Plan for Pandemic Influenza (AHMPPI~\cite{Health14}).
The scenarios will be regularly updated with emerging evidence specific to
COVID-19.

\section{SEEIIR model parameters}

We consider two scenarios, both of which match the estimated doubling rate of
6.4 days observed in Wuhan China during the initial epidemic phase.
Parameter values for these scenarios are listed in Table~\ref{tbl:seeiir}.
We consider that the true epidemic dynamics are likely to lie between these
two scenarios.

In the first scenario we define the incubation period to be 5.2 days and the
infectious period to be 7.68 days.
This was based on initial estimates that the serial interval was around 7--12
days.

In the second scenario we have incorporated more recent evidence regarding the
serial interval, with several new pre-prints reporting values in the range of
4--6 days.
On the basis of this evidence, we define the incubation period to be 4 days,
followed by 1 day of pre-symptomatic infectiousness and 1 day of symptomatic
infectiousness.
Using these new parameters while continuing to match the estimated doubling
rate of 6.4 days observed in Wuhan China during the initial epidemic phase
leads to a lower \(R_0\) value of 1.72.

\begin{table}[H]
  \centering
  \begin{tabular}{lrr}
    \toprule
    Parameter & Scenario 1 & Scenario 2 \\
    \midrule
    \(R_0\) & 2.68 & 1.72 \\
    First incubation period (\(E_1\)), days & 3.2 & 2 \\
    Second incubation period (\(E_2\)), days & 2 & 2 \\
    First infectious period (\(I_1\)), days & 2 & 1 \\
    Second infectious period (\(I_2\)), days & 5.68 & 1 \\
    Quarantine: first infectious period (\(I_1^q\)), days & 1 & 1 \\
    Quarantine: second infectious period (\(I_2^q\)), days & 6.68 & 1 \\
    \bottomrule
  \end{tabular}
  \caption{Parameter values for the SEEIIR model in each epidemic scenario.}
  \label{tbl:seeiir}
\end{table}

\section{Case severity parameters}

We assume that the proportion of infected individuals who will require
hospitalisation (\(\eta\), ``severe'' cases) varies from 1\% to 10\% in the
general population, and that this proportion is five times greater in at-risk
groups (i.e., from 5\% to 50\%).
Of the remaining infections, we assume that a proportion \(\alpha_m\) will
present to outpatient settings but will not require hospitalisation (``mild''
cases).

We sample \(\eta\) from a log-uniform distribution and, for each sampled
value, define a corresponding distribution from which we sample \(\alpha_m\),
as per Moss et al.~\cite{Moss16c}, and obtain the overall presenting proportion
\(\alpha\) for each population group:

\begin{align}
  x &\sim \mathcal{U}(-2, -1) \\
  \eta &= 10^x
    \times \operatorname{RR}(\mathrm{severe}) \\
  \alpha_m &\sim \min(\alpha_m)
    + \left[\max(\alpha_m) - \min(\alpha_m)\right]
    \times \operatorname{Beta}(\mu = 0.5, \mathrm{Var} = 0.2) \\
  \min(\alpha_m) &= 0.05 + 0.2 \cdot \frac{\eta - 0.001}{0.099} \\
  \max(\alpha_m) &= 0.15 + 0.6 \cdot \frac{\eta - 0.001}{0.099} \\
  \alpha &= \eta + (1 - \eta) \cdot \alpha_m
\end{align}

The lower and upper bounds for \(\alpha_m\) are both linear functions of
\(\eta\).
As the proportion of infected individuals who require hospitalisation
increases, so too does the proportion of infected individuals will present to
outpatient settings (but will not require hospitalisation).
We assume a severity pyramid where all deaths observed in the healthcare
system occur in ICUs.
The proportion of severe cases that require ICU care, and the proportion of
these ICU cases that result in deaths, are currently based on values used previously for pandemic influenza, and are listed in Table~\ref{tbl:sev}.
These values will be updated as emerging evidence for COVID-19 becomes
available.

\begin{table}
  \centering
  \begin{tabular}{lrr}
    \toprule
    Parameter & General population & High-risk groups \\
    \midrule
    Require ICU & 12.5\% & 25\% \\
    Death in ICU & 40\% & 60\% \\
    Death without ICU admission & 70\% & 80\% \\
    \bottomrule
  \end{tabular}
  \caption{Case severity parameters for the ``severe'' cases that require
    hospitalisation.
    We assume a severity pyramid where all deaths observed in the healthcare
    system occur in ICUs.
    Where a patient that requires ICU admission cannot be allocated an ICU
    bed, we assume that their survival rate is halved.}
  \label{tbl:sev}
\end{table}

\bibliographystyle{plainrgmc}
\bibliography{refs}

\end{document}

%% RUN_TEST
%
% R0, severity (1..3), pre_symp (0, 1), redn (0, 0.25, 0.50), qtn (0.5, 1), samples
%
% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function run_test(R0, severity, pre_symp, redn, qtn, samples)
    if nargin < 1
        R0 = 2.68;
    end
    if nargin < 2
        severity = 3; % High
    end
    if nargin < 3
        pre_symp = 0; % No pre-symptomatic infectiousness.
    end
    if nargin < 4
        redn = 0.0; % No case management.
    end
    if nargin < 5
        qtn = 0.0; % No quarantine for case contacts.
    end
    if nargin < 6
        samples = 1;
    end

    [model, dists, samples, param1, prev_pwd] = master(R0, severity, ...
                                                       pre_symp, redn, ...
                                                       qtn, ...
                                                       samples);
    [mat_file] = lhs(model, dists, samples, param1);

    data = most_recent('run_test');

    Ns = data.lhs_data.unsampled_params.pi * data.lhs_data.unsampled_params.N;

    fprintf('Plotting daily incidence in both strata ...\n');
    di = get_stat(data, 'DailyIncidence');
    figure();
    di_ind = squeeze(sum(di(1, 1:3, :), 2));
    di_ni = squeeze(sum(di(1, 4:9, :), 2));
    plot((1:366) / 7, di_ind, (1:366) / 7, di_ni);
    legend('Indigenous', 'Non-Indigenous');
    ylabel('Daily incidence');

    fprintf('Plotting cumulative recoveries ...\n');
    R = get_stat(data, 'R');
    RM = get_stat(data, 'RM');
    R_ind = squeeze(sum(R(1, 1:3, :), 2));
    R_ni = squeeze(sum(R(1, 4:9, :), 2));
    RM_ind = squeeze(sum(RM(1, 1:3, :), 2));
    RM_ni = squeeze(sum(RM(1, 4:9, :), 2));
    figure();
    plot((1:366) / 7, R_ind + R_ni, ...
         (1:366) / 7, RM_ind + RM_ni);
    legend('R', 'RM');
    ylabel('Cumulative recoveries');

    fprintf('Plotting cumulative attack rates ...\n');
    N_ind = sum(Ns(1:3));
    N_ni = sum(Ns(4:9));
    figure();
    plot((1:366) / 7, 100 * (R_ind + RM_ind) / N_ind, ...
         (1:366) / 7, 100 * (R_ni + RM_ni) / N_ni);
    legend('Indigenous', 'Non-Indigenous');
    ylabel('Attack Rate (%)');

    final_sizes = round(R(1, :, end) + RM(1, :, end));
    final_sizes ./ Ns';
    attack_rates = 100 * final_sizes ./ Ns';
    for i = 1:length(Ns)
      fprintf('%d cases out of %d population(%d); %0.2f%%\n', ...
              final_sizes(i), Ns(i), i, attack_rates(i));
    end

    %% Plot contacts of managed and non-ascertained cases.
    CT_m = get_stat(data, 'CT_m');
    CT_nm = get_stat(data, 'CT_nm');
    figure();
    plot(1:366, squeeze(sum(CT_m(1, :, :))), ...
         1:366, squeeze(sum(CT_nm(1, :, :))));
    legend('CT_m', 'CT_{nm}');

    %% Plot mild and severe presentations.
    dp = get_stat(data, 'DailyPresentations');
    dp_net = squeeze(sum(dp(1, :, :), 2));
    dp_mild = get_stat(data, 'DailyPresentationsMild');
    dp_mild = squeeze(sum(dp_mild(1, :, :), 2));
    dp_sev = get_stat(data, 'DailyPresentationsSevere');
    dp_sev = squeeze(sum(dp_sev(1, :, :), 2));
    figure();
    plot((1:366) / 7, dp_net, ...
         (1:366) / 7, dp_mild, ...
         (1:366) / 7, dp_sev);
    legend('All presentations', 'Mild', 'Severe');
    ylabel('Daily presentations');

    %% Plot daily presentations for each sample (simulation).
    figure();
    dp = squeeze(sum(dp, 2));
    plot(1:366, dp(:, :))
    ylabel('Daily presentations');

    %% Restore the previous working directory.
    cd(prev_pwd);
end

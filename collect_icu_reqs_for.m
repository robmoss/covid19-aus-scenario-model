% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function out = collect_icu_reqs_for(data, moc)
    if nargin < 3
        weeks = 1;
    end

    %% Calculate the median, the 25% and 75%, and the 5% and 95% centiles.
    qtls = [0.50, 0.25, 0.75, 0.05, 0.95];

    %% Extract the daily numbers of severe presentations.
    %% This will be the metric by which we will select an individual
    %% simulation for each centile.
    di_sev = get_stat(data, 'DailyPresentationsSevere');

    %% Pick the simulations that match the final size centiles.
    final_sizes = squeeze(sum(sum(di_sev, 2), 3));
    fs_qtls = quantile(final_sizes, qtls);
    ixs = zeros(size(fs_qtls));
    for i = 1:length(fs_qtls)
        %% Find the simulations whose final sizes are closest to the centile.
        diffs = abs(final_sizes - fs_qtls(i));
        matches = find(diffs == min(diffs));
        %% Select the first simulation, since there may be multiple.
        ixs(i) = matches(1);
    end

    %%
    %% Now calculate the weekly numbers for each simulation.
    %% NOTE: scale daily incidence by the population fraction.
    %%
    di_sev = di_sev(ixs, :, :);
    if abs(1.0 - moc.popn_frac) > 1e-7
        di_sev = moc.popn_frac * di_sev;
    end

    %% NOTE: record the true demand for ICU admission; note that this is
    %% incidence, **not** prevalence.
    need_icu = di_sev .* reshape(moc.ward_to_ICU, [1 size(moc.ward_to_ICU)]);
    daily_need_icu = squeeze(sum(need_icu, 2));

    %% NOTE: calculate the true demand for ICU admission for under-80s.
    u80_ixs = [1:8 10:17];
    daily_u80_need_icu = squeeze(sum(need_icu(:, u80_ixs, :), 2));
    o80_ixs = [9 18];
    daily_h80_need_icu = daily_u80_need_icu + 0.5 * squeeze(sum(need_icu(:, o80_ixs, :), 2));

    %% Calculate the daily number of presentations that require ICU beds.
    out = struct();
    out.daily_net_icu = daily_need_icu;
    out.daily_u80_net_icu = daily_u80_need_icu;
    out.daily_h80_net_icu = daily_h80_need_icu;
end

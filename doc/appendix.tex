\documentclass[DIV14]{scrartcl}

\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{amsmath}
%% Allow URLs to be split across lines at hyphens.
\PassOptionsToPackage{hyphens}{url}
\usepackage{hyperref}

\hypersetup{
  colorlinks=true,
  citecolor=blue!70!black,
  linkcolor=blue!70!black,
  urlcolor=blue!70!black
}

\graphicspath{{figures/}}

\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{arrows.meta}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{positioning}
\usetikzlibrary{shapes.multipart}

\usepackage[square,numbers]{natbib}

\DeclareMathOperator{\Inf}{Inf}
\DeclareMathOperator{\Hosp}{Hosp}
\DeclareMathOperator{\ICU}{ICU}

\title{Appendix: Model description}
\author{}
\date{}

\begin{document}

\maketitle

\section{Epidemic model description}

The model structure is shown in Figure~\ref{fig:model}.
Model compartments are described in Table~\ref{tbl:comps}, model parameters
are defined in Table~\ref{tbl:params}, and population sub-groups are listed in
Table~\ref{tbl:strata}.

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.8\textwidth]{model_diagram}
  \caption{Model diagram.
    Some proportion \(p_M\) of presenting cases are ascertained and
    isolated.
    Quarantined persons (shown with dashed borders) exert a lesser force of
    infection than non-quarantined persons.}
  \label{fig:model}
\end{figure}

\begin{table}
  \centering
  \begin{tabular}{p{8cm}rr}
    \toprule
    Description & General & Quarantined \\
    \midrule
    Susceptible individuals & \(S\) & --- \\
    Latent period (first stage) & \(E_1\) & \(E_1^q\) \\
    Latent period (second stage) & \(E_2\) & \(E_2^q\) \\
    Infectious period (first stage) & \(I_1\) & \(I_1^q\) \\
    Infectious period (second stage) & \(I_2\) & \(I_2^q\) \\
    Recovered individuals & \(R\) & \(R^q\) \\
    Managed cases, ascertained upon leaving \(I_1\) and less infectious than
    individuals in \(I_2\) & \(M\) & \(M^q\) \\
    Recovered individuals that were managed cases & \(R_M\) & \(R_M^q\) \\
    \midrule
    Contacts of unmanaged cases & \multicolumn{2}{c}{\(CT_\mathrm{NM}\)} \\
    Contacts of managed cases, who will enter \(E_1^q\) if they become infected
    & \multicolumn{2}{c}{\(CT_\mathrm{M}\)} \\
    \bottomrule
  \end{tabular}
  \caption{Model compartments for the general population (middle column) and
    for individuals who were quarantined as a result of contact tracing (right
    column).}
  \label{tbl:comps}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{rl}
    \toprule
    & Definition \\
    \midrule
    \(\sigma_1\) & Inverse of first latent period. \\
    \(\sigma_2\) & Inverse of second latent period. \\
    \(\gamma_1\) & Inverse of first infectious period. \\
    \(\gamma_2\) & Inverse of second infectious period. \\
    \(\gamma_1^q\) & Inverse of first infectious period for quarantined cases. \\
    \(\gamma_2^q\) & Inverse of second infectious period for quarantined cases. \\
    \(\eta\) & Scaling factor for hospitalisation proportion (``severe''). \\
    \(\alpha_m\) & Proportion of non-severe people who present (``mild''). \\
    \(\alpha\) & Net proportion of people who present. \\
    \(R_0\) & The basic reproduction number. \\
    \(\lambda\) & The net force of infection. \\
    \(\lambda_{imp}\) & The force of infection from importation. \\
    \(\beta\) & The force of infection exerted by one individual. \\
    \(\kappa\) & The per-person contact rate (20 people per day). \\
    \(\delta\) & The duration of quarantine for contacts (14 days). \\
    \(p_M\) & Probability of presenting cases being effectively
    managed\(^\dag\). \\
    \(Q_\mathrm{eff}\) & The reduction in infectiousness due to
    quarantine\(^\dag\). \\
    \(M_\mathrm{eff}\) & The reduction in infectiousness due to case
    management\(^\dag\). \\
    \(\rho\) & The proportion of contacts (of ascertained cases) that will
    self-quarantine\(^\dag\). \\
    \bottomrule
  \end{tabular}
  \caption{Model parameters; key intervention parameters are marked with
    \(\dag\).}
  \label{tbl:params}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{lrrr}
    \toprule
    Age & Indigenous & Non-Indigenous
    & \(\Pr(\Hosp | \Inf)\) \\
    \midrule
     0--9  & 184,560 & 2,966,400 & 0.062\% \\
    10--18 & 149,040 & 2,466,480 & 0.062\% \\
    19--29 & 151,440 & 3,651,120 & 0.775\% \\
    30--39 &  93,360 & 3,315,360 & 2.900\% \\
    40--49 &  87,360 & 3,154,560 & 5.106\% \\
    50--59 &  66,960 & 2,964,720 & 9.895\% \\
    60--69 &  38,880 & 2,397,120 & 15.493\% \\
    70--79 &  15,360 & 1,423,440 & 35.762\% \\
    80+    &   5,280 &   868,560 & 65.936\% \\
    \bottomrule
  \end{tabular}
  \caption{Population groups by age and Indigenous status, showing population
    sizes, and the probability of requiring hospitalisation given infection.
    Demographic breakdown as per Australian Bureau of Statistics resident
    population estimates, catalogue number 3238.0.55.001, June 2016.
    The values of \(\Pr(\Hosp | \Inf)\) are upper bounds; we defined the lower
    bounds to be half of these listed values.}
  \label{tbl:strata}
\end{table}

\begin{align}
  \frac{dS}{dt} &= - \frac{S}{N} \cdot \lambda \\
  \frac{dE_1}{dt} &= \lambda \left(\frac{S}{N} - \rho \Theta_\mathrm{M}\right)
    - \sigma_1 E_1
  & \frac{dE_1^q}{dt} &= \lambda \rho \Theta_\mathrm{M} - \sigma_1 E_1^q \\
  \frac{dE_2}{dt} &= \sigma_1 E_1 - \sigma_2 E_2
  & \frac{dE_2^q}{dt} &= \sigma_1 E_1^q - \sigma_2 E_2^q \\
  \frac{dI_1}{dt} &= \sigma_2 E_2 - \gamma_1 I_1
  & \frac{dI_1^q}{dt} &= \sigma_2 E_2^q - \gamma_1^q I_1^q \\
  \frac{dI_2}{dt} &= \gamma_1 I_1 (1 - \alpha p_M) - \gamma_2 I_2
  & \frac{dI_2^q}{dt} &= \gamma_1^q I_1^q (1 - \alpha p_M) - \gamma_2^q I_2^q \\
  \frac{dR}{dt} &= \gamma_2 I_2
  & \frac{dR^q}{dt} &= \gamma_2^q I_2^q \\
  \frac{dM}{dt} &= \gamma_1 I_1 (\alpha p_M) - \gamma_2 M
  & \frac{dM^q}{dt} &= \gamma_1^q I_1^q (\alpha p_M) - \gamma_2^q M^q \\
  \frac{dR_M}{dt} &= \gamma_2 M
  & \frac{dR_M^q}{dt} &= \gamma_2^q M^q
\end{align}

\begin{align}
  \alpha &= \eta + \alpha_m (1 - \eta) \\
  \beta &= R_0 \times \left[
    (\sigma_2)^{-1} + (\gamma_1)^{-1} + (\gamma_2)^{-1} \right]^{-1} \\
  \lambda &= \lambda_{imp} + \beta (I_1 + I_2)
    + \beta \cdot (1 - Q_\mathrm{eff}) (I_1^q + I_2^q) \\
    & \phantom{= 1} + \beta \cdot (1 - M_\mathrm{eff}) \cdot M
    + \beta_{M\vert{}Q} \cdot M^q \notag \\
  \beta_{M\vert{}Q} &= \beta \cdot [1 - \max\left(M_\mathrm{eff}, Q_\mathrm{eff}\right)] \\
  \frac{dCT_\mathrm{M}}{dt} &= \kappa \cdot (\gamma_1 I_1 + \gamma_1^q I_1^q)
    \cdot (\alpha \cdot p_M)
    - \delta CT_\mathrm{M}
    - \lambda \cdot \Theta_\mathrm{M} \\
  \frac{dCT_\mathrm{NM}}{dt} &= \kappa \cdot (\gamma_1 I_1 + \gamma_1^q I_1^q)
    \cdot (1 - \alpha \cdot p_M)
    - \delta CT_\mathrm{NM}
    - \lambda \cdot \Theta_\mathrm{NM} \\
  \Theta_\mathrm{M} &= \frac{S}{N} \cdot \frac{CT_\mathrm{M}}{%
    CT_\mathrm{M} + CT_\mathrm{NM}} \\
  \Theta_\mathrm{NM} &= \frac{S}{N} \cdot \frac{CT_\mathrm{NM}}{%
    CT_\mathrm{M} + CT_\mathrm{NM}}
\end{align}

\clearpage

\section{Epidemic scenarios}

\subsection{Transmission assumptions}

We base our transmission assumptions on initial estimates of a doubling time
of 6.4 days and \(R_0 = 2.68\) from Wuhan~\cite{Wu20}.
In the initial version of this model, we assumed that all transmission
occurred following an incubation period of 5.2 days, within a two-stage
infectious period of 7.68 days required to match the doubling time, \(R_0\),
and latent duration assumptions.
However, as a result of increasing evidence of the importance of
pre-symptomatic transmission~\cite{Ganyani20,Tindale20}, we have revised the
latent period to 3.2 days in order to allow 2 days of pre-symptomatic
transmission.
We have elected to maintain the overall duration of infection and doubling
time, which is consistent with a revised \(R_0 = 2.53\).
The two-stage latent and infectious periods now have durations of 1.6 days
each (latent period), and 4 and 5.68 days respectively (infectious period).
The associated generation interval for this parameterisation is 6 days.

\subsection{ICU and hospitalisation rates}

As of February 12 2020, there had been approximately 1000 severe cases of
COVID19 reported outside Hubei province in China~\cite{NHC0213}.
In order to establish an overall severe case-rate we first extracted the
number of cases (around 11,340) outside Hubei at this time from the China CDC
descriptive epidemiology publication~\cite{NCPERET20}, leading to an overall
severe case rate of 8.8\%.
As severity was not reported by age, we have used other sources to establish
an appropriate age pattern, in particular the recent ICNARC report on 775 ICU
admissions in the UK~\cite{ICNARC20}.
Briefly, we extracted data on the proportion of ICU admissions by age and
gender and then age and gender standardised these using UK 2018 mid-year
population figures~\cite{ONS19}, under the assumption that infection rates in
adults are constant by age up to age 70.
These relative weightings after standardisation and averaging over gender are
0.05 in 20-29, 0.19 in 30-39, 0.33 in 40-49 and 0.64 in 50-59, compared to the
reference 60-69 year group.
This allowed us to compute relative likelihoods of ICU admission by age in
adults up to 70.
We note that male presentations were substantially over-represented in this
data, as reported in other settings but that presentations in individuals
\(> 70\) were substantially less than expected, perhaps reflecting
successful mitigation of transmission to these age-groups in the UK.
Therefore, to establish appropriate baseline values in 60-69, 70-70 and 80+ we
drew instead on the assumptions in Imperial College report 9 and then scaled
values in younger adults using the proportions described above.
For children, we drew on the EpiCentro report of March 26~\cite{ISS20}, in
which 0 of 553 children with data available had been admitted to ICU.
Based on comparisons to notified incidence rates in those \(> 80\), cases in
those \(< 20\) in Italy appear at least \(30 \times\) under-reported in
comparison to population proportions.
Scaling up by \(30 \times\) and applying the rule of
3~\cite{Eypasch95,Ludbrook09}, we estimate an upper bound on ICU risk as
\(1/5530 \approx 0.018\%\), which we apply conservatively as our estimate in
this age group.

In order to compute hospitalisation rates by age, we extracted the
age-distribution of cases outside of China from the CCDC report, and applied
our ICU rates by age, scaled up by a constant factor, so as to match the
overall severe case rate of 8.8\% from that setting.
This exercise led to our assumption that 29\% of hospitalised cases will
require ICU care and is approximately equal to the proportion assumed in
Imperial College Report 9.

\newpage
\subsection{Range of scenarios}

We consider the following scenarios, and provide summary statistics for each
scenario in Table~\ref{tbl:ar}:

\begin{itemize}
\item The mean latent period is 3.2 days, the mean infectious period is
  9.68 days, and the doubling time is 6.4 days.
\item So the baseline \(R_0\) is 2.53, and the mean generation time is 6 days.
\item Symptom onset occurs 2 days after the onset of infectiousness, so the
  mean incubation period is 5.2 days.
\item Case ascertainment occurs 2 days after symptom onset.
\item \(\sigma_1 = \sigma_2 = 1.6\text{ days}\);
  \(\gamma_1 = \gamma_1^q = 4.0\text{ days}\);
  \(\gamma_2 = \gamma_2^q = 5.68\text{ days}\).
\item There is no case isolation, or case isolation reduces transmission by
  80\% (\(M_\mathrm{eff} \in \{0, 0.8\}\)).
\item All presenting cases can be isolated (\(p_M = 1\)).
\item There is no self-quarantine (e.g., due to lack of contact tracing, or
  electing not to promote self-quarantine), or 80\% of contacts will adhere to
  self-quarantine (\(\rho \in \{0, 0.8\}\)).
\item Self-quarantine halves transmission (\(Q_\mathrm{eff} = 0.5\)).
\item Physical distancing measures may reduce \(R_0\) by
  \(25\%\) (\(R = 1.8975\)) or by \(33\%\) (\(R = 1.6867\)).
  We assume these measures will be applied \emph{in addition to}
  self-quarantine and case isolation.
\end{itemize}

\begin{table}[!h]
  \centering
  \include{attackrate}
  \caption{Key epidemic characteristics for each of the scenarios described
    above.
    Median outcomes are reported, with 5th and 95th percentiles shown below in
    brackets.
    \newline
    \(\dagger\): the effective reproduction number in the absence of
    self-quarantine and case isolation.}
  \label{tbl:ar}
\end{table}

\clearpage

\section{Models of care}

\begin{figure}
  \centering
  \begin{tikzpicture}[
      %% Use the same arrow style as LaTeX's \to command in maths mode.
      -{Latex[length=3mm]},
      %% Leave a small gap between the arrow head and the node.
      shorten >=0.05cm,
      %% Draw very thick lines for arrows, etc.
      ultra thick,
      %% Node spacing.
      node distance=2cm]
    \tikzset{%
      every node/.style={%
        %% Minimum node dimensions
        minimum width=1.25cm,minimum height=0.75cm,%
        %% No spacing between inner content and edge, add rounded corners.
        inner sep=0pt,rounded corners=0.25cm,%
        %% Draw node outlines with thick lines.
        thick}}
    %\fontspec{Noto Sans}
    \large

    \node[draw=black,align=center,text width=6cm] (DP) {Daily Presentations};
    \node[draw=black,align=center,text width=1.5cm,
      below=1cm of DP,xshift=-2.25cm] (M) {Mild};
    \node[draw=black,align=center,text width=4cm,
      below=1cm of DP,xshift=1cm] (S) {Severe};
    \node[draw=black,align=center,text width=1.75cm,
      below=1cm of S,xshift=-1.125cm] (E) {Early};
    \node[draw=black,align=center,text width=1.75cm,
      below=1cm of S,xshift=1.125cm] (L) {Late};
    \node[draw=black,align=center,text width=6cm,
      below=4.75cm of DP] (OP) {Outpatient Presentations};
    \node[draw=black,align=center,text width=1.75cm,
      below=1cm of OP,xshift=-0.125cm] (W) {Ward};
    \node[draw=black,align=center,text width=1.75cm,
      below=1cm of OP,xshift=2.125cm] (I) {ICU};
    \node[draw=black,align=center,text width=1.75cm,
      below=1cm of I] (D) {Deaths};

    %% Draw vertical arrows, aligned to the horizontal centre of either the
    %% source or destination node (see https://tex.stackexchange.com/a/87455).
    \draw (DP.south -| M.north) -- (M.north);
    \draw (DP.south -| S.north) -- (S.north);
    \draw (M.south) -- (OP.north -| M.south);
    \draw (S.south -| E.north) -- (E.north);
    \draw (S.south -| L.north) -- (L.north);
    \draw (E.south) -- (OP.north -| E.south);
    \draw (L.south) -- (OP.north -| L.south);
    \draw[shorten >=0.15cm] (OP.south -| I.north) -- (W.north);
    \draw (OP.south -| I.north) -- (I.north);
    \draw (DP.south -| M.north) -- (M.north);
    \draw (I.south) -- (D.north);

    %% Curved arrows for repeat presentations.
    \draw[dashed,shorten >=0.1cm] (OP.north -| M.south) to [out=55,in=135] (OP.north -| E.south west);
    \draw[dashed,shorten >=0.1cm] (OP.north -| E.south) to [out=55,in=135] (OP.north -| L.south);

    %% Ward bed block inhibits outpatient (ED) capacity.
    \draw[draw=black!50!white,-Bar] (W.125) -- (OP.south -| W.125);

  \end{tikzpicture}
  \caption{A schematic of the clinical pathways model.
    Repeat outpatient presentations are shown as dashed arrows.
    As ward bed occupancy increases, ED consultation capacity decreases (grey
    bar) and fewer severe cases can be triaged and admitted.}
  \label{fig:paths}
\end{figure}

The structure of the clinical pathways model is shown in
Figure~\ref{fig:paths}, and is adapted from Moss et al.~\cite{Moss16}.
Some infected individuals will require hospitalisation (``severe cases'') and
of the rest, some will present to outpatient settings (``mild cases'').
The proportion of mild cases that present to hospital EDs rather than to GP
clinics in Australia was estimated to be 20\%, based on expert consultation.
It is further assumed that a fraction of the severe cases will present early
in their clinical course to an outpatient setting, in advance of requiring
hospitalisation.
We assume that a fixed fraction of hospitalised cases require ICU admission.
Parameters that govern these flows are listed in Table~\ref{tbl:moc}.

\begin{table}
  \centering
  \begin{tabular}{lr}
    \toprule
    Parameter & Value \\
    \midrule
    Proportion of mild cases that present to GPs & 80\% \\
    Proportion of mild cases that present to EDs & 20\% \\
    Proportion of mild GP cases that revisit EDs & 10\% \\
    Proportion of mild ED cases that revisit GPs & 5\% \\
    Proportion of severe cases that present early & 50\% \\
    Proportion of early severe cases that present to GPs & 80\% \\
    Proportion of early severe cases that present to EDs & 20\% \\
    Proportion of non-early severe cases that present to EDs & 100\% \\
    Proportion of admitted cases that require ICU & 29.335\% \\
    Mean length of stay in ward beds & 8 days \\
    Mean length of stay in ICU beds & 10 days \\
    Ward bed availability threshold for reducing ED capacity & 20\% \\
    Minimum ED consultation capacity & 10\% \\
    \bottomrule
  \end{tabular}
  \caption{Parameters that characterise patient flows through the clinical
    pathways model.}
  \label{tbl:moc}
\end{table}

We assume that a proportion of infected individuals (\(\alpha_s\)) will
require hospitalisation, and that this proportion varies by age.
The upper bounds for each age group are listed in Table~\ref{tbl:strata}.
A further proportion of infected individuals (\(\alpha_m\)) will present to
outpatient settings but will not require hospitalisation (``mild'' cases).
We introduce a scaling factor \(\eta\) from which we calculate \(\alpha_s\),
and define the sampling distribution for this mild proportion as per Moss et
al.~\cite{Moss16}:

\begin{align}
  \eta_\text{pow} &\sim \mathcal{U}(\log_{10} 0.5, \log_{10}1.0) \\
  \eta &= 10^{\eta_\text{pow}} \\
  \alpha_m &\sim \min(\alpha_m)
    + \left[\max(\alpha_m) - \min(\alpha_m)\right]
    \times \operatorname{Beta}(\mu = 0.5, \mathrm{Var} = 0.2) \\
  \min(\alpha_m) &= 0.05 + 0.2 \cdot \frac{\eta - 0.01}{0.99} \\
  \max(\alpha_m) &= 0.15 + 0.6 \cdot \frac{\eta - 0.01}{0.99} \\
  \alpha_s &= \eta \cdot \Pr(\Hosp | \Inf) \\
  \alpha &= \alpha_s + (1 - \alpha_s) \cdot \alpha_m
\end{align}

The lower and upper bounds for \(\alpha_m\) are both linear functions of
\(\eta\).
As the proportion of infected individuals who require hospitalisation
increases, so too does the proportion of infected individuals will present to
outpatient settings (but will not require hospitalisation).

National consultation and admission capacities for each health care setting
were informed by public reports of Australian health care infrastructure,
under the assumption that in a worst-case scenario up to 50\% of total
capacity in each health care setting could possibly be devoted to Covid-19
patients, and are listed in Table~\ref{tbl:caps}.
Patients are admitted to general wards with a mean length of stay of 8 days,
and are admitted to ICUs with a mean length of stay of 10 days.
Therefore, it is the prevalence of cases requiring hospitalisation that
determines the available ward and ICU bed capacities for new admissions.
At a jurisdictional level, daily presentations are allocated in proportion to
each jurisdiction's resident population.
Health care capacity is determined based on the numbers of full time GPs per
jurisdiction, the yearly number of ED presentations per jurisdiction, the
number of overnight beds available in public hospitals by jurisdiction, and
the number of ICU beds per jurisdiction, as described in the AIHW report
``Hospital resource 2017--18: Australian hospital statistics''~\cite{AIHW19}.

In the event that there is insufficient capacity in a healthcare setting for a
person to receive a consultation or to be admitted to hospital, the following
steps are applied:

\begin{enumerate}
\item Severe cases that cannot receive an ED consultation (or a consultation
  with an alternate care pathway, if present) are not observed by the
  healthcare system, and are reported as excess demand in this care setting.
\item Mild cases that cannot receive an ED or GP consultation (or a
  consultation with an alternate care pathway, if present) are not observed by
  the healthcare system, and are reported as excess demand in this care
  setting.
\item Any severe cases that require ICU admission, but cannot be admitted due
  to a lack of available ICU beds, are considered for admission to a general
  ward and are reported as excess ICU demand.
\item Any severe cases that cannot be admitted to a general ward to a lack of
  available ward beds are observed by the healthcare system, and are reported
  as excess ward demand.
\end{enumerate}

\begin{table}
  \centering
  \begin{tabular}{lrrrrrrrrr}
    \toprule
    & National & ACT & NSW & NT & QLD & SA & TAS & VIC & WA \\
    \midrule
    ICU beds & 1,114 & 22 & 437 & 11 & 206 & 94 & 25 & 238 & 81 \\
    Ward beds & 25,756 & 448 & 8,832 & 276 & 5,099 & 1,915 & 557 & 6,158 & 2,471 \\
    ED consultations & 10,935 & 202 & 3,945 & 172 & 2,071 & 694 & 222 & 2,456 & 1,173 \\
    GP consultations & 202,999 & 2,607 & 66,616 & 1,582 & 43,627 & 14,005 & 3,935 & 51,338 & 19,289 \\
    \bottomrule
  \end{tabular}
  \caption{Estimated national and per-jurisdiction healthcare capacities,
    under the assumption that 50\% of total capacity in each healthcare
    setting could possibly be devoted to Covid-19 patients.
    ED and GP capacities reflect maximum number of daily consultations.}
  \label{tbl:caps}
\end{table}

\subsection{Service substitution models}

We consider two service-substitution models of care to circumvent EDs as the
sole pathway for hospital admission:

\begin{description}
\item[Covid-19 Clinics] dedicated COVID-19 clinics for triage and hospital
  admission; and
\item[Phone/Online] an alternative triage system to bypass GP/ED for hospital
  admission.
\end{description}

We assume that COVID-19 clinics are staffed by 10\% of the GP and ED
workforce, and that for each GP/ED consultation lost due to this decrease in
staffing, two clinic consultations are gained.
This is due to the assumption that while only 50\% of GP and ED consultations
may be allocated to potential COVID-19 cases, every clinic consultation is
allocated to a potential COVID-19 case.

We assumed that a telephone and/or online consultation service could be
staffed without materially diminishing the GP and ED work forces.
The capacity of this service model was defined as 100,000 consults per day, a
coarse estimate that is comparable to the consultation rate of the National
Pandemic Flu Service in the United Kingdom in 2009 (around 135,000 consults
per day~\cite{Rutter14}).

When one of these alternate services is provided, we assume that 25\% of mild
cases will use it in lieu of EDs and GPs, and that severe cases place equal
demand on EDs and on these alternative services.

\subsection{Critical care expansion}

Recall that in the base care, COVID-19 patients have access to half of all
ICU beds in the healthcare system.
We consider two scenarios where ICU bed capacity is expanded:

\begin{description}
\item[Moderate] the number of ICU beds available to COVID-19 patients is
  doubled compared to the base case (i.e., 150\% of total baseline ICU bed
  capacity); and
\item[Large] the number of ICU beds available to COVID-19 patients is
  tripled compared to the base case (i.e., 200\% of total baseline ICU bed
  capacity); and
\item[Extreme] the number of ICU beds available to COVID-19 patients is
  increased 5-fold compared to the base case (i.e., 300\% of total baseline
  ICU bed capacity).
\end{description}

\bibliographystyle{plainrgmc}
\bibliography{appendix}

\end{document}

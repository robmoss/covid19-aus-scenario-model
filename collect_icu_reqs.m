%% COLLECT_ICU_REQS(output_file)
%
% Collect the daily incidence of cases that require an ICU bed for individual
% simulations, selected according to their final size (median and 5th, 25th,
% 75th, and 95th centiles).
%
% Note that this reports the *true* requirements, because it does not consider
% the clinical pathways model and so ignores triage capacity constraints.

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function collect_icu_reqs(output_file)
    if nargin < 1
        output_file = 'icu_demand.csv';
    end

    sweep_name = 'default';
    vals = parameter_sweep(sweep_name);

    %% Write to an empty file and print the column titles.
    fileID = fopen(output_file, 'w');
    fprintf(fileID, '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n', ...
            'R0', 'Severity', ...
            'Pm', 'Qeff', 'Meff', 'rho', ...
            'Jurisdiction', 'Ages', 'day', ...
            'median', 'CI25', 'CI75', 'CI05', 'CI95');
    formatSpec = '%.2f,%d,%d,%.1f,%.1f,%.1f,%s,%s,%d,%f,%f,%f,%f,%f\n';

    moc = model_of_care('default', 'moderate');

    %% Loop over every scenario.
    for R0 = vals.R0
        for sev = vals.Severity
            for pm = vals.Pm
                for qeff = vals.Qeff
                    for meff = vals.Meff
                        for rho = vals.rho
                            if rho ~= meff
                                %% NOTE: only consider scenarios where either
                                %% the epidemic is unmitigated, or both case
                                %% isolation and self-quarantine measures are
                                %% in place.
                                continue
                            end
                            if vals.R0_values(R0) < 2.53 && rho == 0
                                %% NOTE: skip social distancing scenarios with
                                %% no case isolation and self-quarantine.
                                continue
                            end

                            exp_name = sprintf(vals.exp_fmt, R0, sev, pm, qeff, meff, rho);
                            exp_name = strrep(exp_name, '.', '');
                            exp_name = strrep(exp_name, '00', '0');
                            data = most_recent(exp_name);

                            out = collect_icu_reqs_for(data, moc);
                            t_max = size(out.daily_net_icu, 2);

                            for t = 1:t_max
                                fprintf(fileID, formatSpec, ...
                                        vals.R0_values(R0), ...
                                        sev, pm, qeff, meff, rho, ...
                                        moc.jurisdiction, 'All ages', ...
                                        t, ...
                                        out.daily_net_icu(:, t));
                                fprintf(fileID, formatSpec, ...
                                        vals.R0_values(R0), ...
                                        sev, pm, qeff, meff, rho, ...
                                        moc.jurisdiction, '0-79', ...
                                        t, ...
                                        out.daily_u80_net_icu(:, t));
                                fprintf(fileID, formatSpec, ...
                                        vals.R0_values(R0), ...
                                        sev, pm, qeff, meff, rho, ...
                                        moc.jurisdiction, '50% of 80+', ...
                                        t, ...
                                        out.daily_h80_net_icu(:, t));
                            end
                        end
                    end
                end
            end
        end
    end

    fclose(fileID);
end

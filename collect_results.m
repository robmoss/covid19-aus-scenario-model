%% COLLECT_RESULTS(output_file, moc_names, ppe_recs, juris_names)
%
% Collect summary statistics for all nCoV scenarios, for multiple models of
% care and multiple PPE recommendations. By default, this will write results
% to "results.csv" for all models of care and all PPE recommendations, at a
% national level and for each jurisdiction.
%
%See also collect_results_for, model_of_care, ppe_usage

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function collect_results(output_file, moc_names, ppe_recs, juris_names)
    if nargin < 1
        output_file = 'results.csv';
    end
    if nargin < 2
        moc_names = {'default', 'double_icu', '3x_icu', '5x_icu', 'clinics'};
    end
    if nargin < 3
        ppe_recs = {'moderate'};
    end
    if nargin < 4
        juris_names = {};
    end

    sweep_name = 'default';
    vals = parameter_sweep(sweep_name);

    %% Write to an empty file and print the column titles.
    fileID = fopen(output_file, 'w');
    fprintf(fileID, '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n', ...
            'R0', 'Severity', ...
            'Pm', 'Qeff', 'Meff', 'rho', ...
            'ModelOfCare', 'PPE_rec', 'Jurisdiction', ...
            'stat', 'median', 'CI05', 'CI95', 'mean');
    formatSpec = '%.2f,%d,%d,%.1f,%.1f,%.1f,%s,%s,%s,%s,%f,%f,%f,%f\n';

    %% Loop over every scenario.
    for R0 = vals.R0
        for sev = vals.Severity
            for pm = vals.Pm
                for qeff = vals.Qeff
                    for meff = vals.Meff
                        for rho = vals.rho
                            if rho ~= meff
                                %% NOTE: only consider scenarios where either
                                %% the epidemic is unmitigated, or both case
                                %% isolation and self-quarantine measures are
                                %% in place.
                                continue
                            end
                            if vals.R0_values(R0) < 2.53 && rho == 0
                                %% NOTE: skip social distancing scenarios with
                                %% no case isolation and self-quarantine.
                                continue
                            end
                            exp_name = sprintf(vals.exp_fmt, R0, sev, pm, qeff, meff, rho);
                            exp_name = strrep(exp_name, '.', '');
                            exp_name = strrep(exp_name, '00', '0');
                            data = most_recent(exp_name);

                            %% Write the results for this scenario, for
                            %% each of the models of care and PPE usage
                            %% recommendations.
                            for moc_name = moc_names
                                for ppe_rec = ppe_recs
                                    moc = model_of_care(moc_name{:}, ppe_rec{:});
                                    results = collect_results_for(data, moc);

                                    nrows = length(results.stat_names);
                                    for r = 1:nrows
                                        if length(results.values(r, :)) ~= 4
                                            error('Invalid results for "%s".', results.stat_names{r})
                                        end
                                        fprintf(fileID, formatSpec, ...
                                                vals.R0_values(R0), ...
                                                sev, pm, qeff, meff, rho, ...
                                                moc_name{:}, ppe_rec{:}, ...
                                                moc.jurisdiction, ...
                                                results.stat_names{r}, ...
                                                results.values(r, :));
                                    end

                                    %% Also calculate the results for each jurisdiction.
                                    for i = 1:length(juris_names)
                                        moc = model_of_care(moc_name{:}, ppe_rec{:}, ...
                                                            juris_names{i});
                                        results = collect_results_for(data, moc);

                                        nrows = length(results.stat_names);
                                        for r = 1:nrows
                                            if length(results.values(r, :)) ~= 4
                                                error('Invalid results for "%s".', results.stat_names{r})
                                            end
                                            fprintf(fileID, formatSpec, ...
                                                    vals.R0_values(R0), ...
                                                    sev, pm, qeff, meff, rho, ...
                                                    moc_name{:}, ppe_rec{:}, ...
                                                    moc.jurisdiction, ...
                                                    results.stat_names{r}, ...
                                                    results.values(r, :));
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end

    fclose(fileID);
end

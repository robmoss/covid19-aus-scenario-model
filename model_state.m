%% Initial Model State
%% Calculates the initial state of the model; the state variables are
%% collected into a single array.
%%

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [state] = model_state(params)
    %% Initial exposures.
    E1 = round(params.E1_init * params.pi);
    E2 = round(params.E2_init * params.pi);

    %% Susceptible population.
    S = params.N .* params.pi - E1 - E2;

    %% Remaining compartments.
    n = length(params.pi);
    I1 = zeros(n, 1);
    I2 = zeros(n, 1);
    M = zeros(n, 1);
    R = zeros(n, 1);
    RM = zeros(n, 1);
    %% Contact tracing.
    CT_m = zeros(n, 1);
    CT_nm = zeros(n, 1);
    %% Quarantined cases
    E1q = zeros(n, 1);
    E2q = zeros(n, 1);
    I1q = zeros(n, 1);
    I2q = zeros(n, 1);
    Rq = zeros(n, 1);
    Mq = zeros(n, 1);
    RMq = zeros(n, 1);

    %% State variable vector
    state = [S; E1; E2; I1; I2; R; M; RM; CT_m; CT_nm; ...
             E1q; E2q; I1q; I2q; Rq; Mq; RMq; ];
end

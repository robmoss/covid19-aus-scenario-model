%% Run simulations for a single scenario.

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [mat_file] = scen_R01_sev4_pm1_qeff05_meff08_rho08(samples)
  vals = parameter_sweep();
  R0 = vals.R0_values(1);
  severity = 4;
  pm = 1;
  qeff = 0.5;
  meff = 0.8;
  rho = 0.8;

  if nargin < 1
    samples = 200;
  end

  [model, dists, samples, param1, prev_pwd] = master(R0, severity, pm, qeff, meff, rho, samples);

  %% Run the simulation and load the results.
  [mat_file] = lhs(model, dists, samples, param1);

  %% Restore the previous working directory.
  cd(prev_pwd);
end

%% RUN_ALL(workers)
%
% Runs all of the scenario simulations, using the specified number
% of parallel workers (default: do not use any parallel workers).

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function run_all(workers)
    if nargin > 0
        parpool(workers)
    end
    scen_R01_sev4_pm1_qeff05_meff0_rho0();
    scen_R01_sev4_pm1_qeff05_meff08_rho08();
    scen_R02_sev4_pm1_qeff05_meff08_rho08();
    scen_R03_sev4_pm1_qeff05_meff08_rho08();
end

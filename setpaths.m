% Copyright 2016 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function setpaths()
  addpath('../lhs_framework')
  addpath('../lhs_framework/solvers')
  addpath('.')
  addpath('./experiments')

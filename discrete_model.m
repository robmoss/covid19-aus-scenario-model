%%DISCRETE_MODEL   Packages the antiviral SEIR model for LHS experiments.
%%   The LHS code requires each model to be represented as a record (structure
%%   array). This function packages the various SEIR model functions into this
%%   required form.
%%
%%   model = DISCRETE_MODEL();
%%
%%   MODEL is a record (structure array) that contains the model details (see
%%   EXAMPLE_SIR_MODEL for a simple example).
%%
%%See also example_SIR_model, example_LHS_experiment, lhs

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [model] = discrete_model(fixed_params)
    if nargin < 1
        fixed_params = struct();
    end
    model = struct( ...
        'params', @() model_params(fixed_params), ...
        'state', @model_state, ...
        'events', @init_events, ...
        'dependent_dists', @dep_dists, ...
        'statistics', @model_statistics, ...
        'default_dists', @default_dists, ...
        'equations', @discrete_rhs);
end

%%DEP_DISTS   Returns probability distributions for dependent model parameters.
%%   The antiviral SEIR model contains several dependent parameters, whose
%%   values are derived from (or influenced by) independent parameters.
%%
%%   dists = DEP_DISTS(params);
%%
%%   PARAMS is a record (structure array) that defines the value of each
%%   independent model parameter.
%%
%%   DISTS is a record (structure array) that associates a probability
%%   distribution with each dependent model parameter.
function [dists] = dep_dists(params)
    if nargin == 0
      dists = {'alpha_m', 'R0scaling'};
        return;
    end
    eta = params.eta' * 100; % scale eta to [0.1, 10]
    scale = (eta - 0.1) * 0.8 / 9.9 + 0.2; % scale eta to [0.2, 1.0]
    mmean = scale * 2 / 4;  % mean is [0.1,  0.5 ]
    minv = scale * 1 / 4;  % min is  [0.05, 0.25], max is  [0.15, 0.75]
    maxv = scale * 3 / 4;  % ie, restrict to mean +/- (mean/2)
    varn = scale * 1 / 10; % variance is [0.02, 0.1]

    %% Construct the appropriate beta distribution.
    dist_alpha_m = dist_beta(mmean, varn, minv, maxv);

    %% NOTE: scaling factor to convert between NGM and R0.
    n = length(params.pi);
    net_infectious_period = 1 ./ params.gamma_1 + 1 ./ params.gamma_2;
    net_inf_rate = 1 ./ net_infectious_period;
    m_eig = max(eig((params.kappa .* params.tpm) ./ repmat(net_inf_rate,1,n)));
    R0scaling = dist_const(params.R0 / m_eig);

    dists = struct(...
                    'alpha_m', dist_alpha_m, ...
                    'R0scaling', R0scaling ...
                  );
end

%%INIT_EVENTS   Initialises the events record for a simulation.
%%   During each model simulation, various events and conditions can arise. This
%%   function returns an initialised record (structure array) for recording any
%%   such events that occur during a single simulation.
%%
%%   events = INIT_EVENTS(init_state, init_params);
%%
%%   INIT_STATE is a vector that specifies the initial value of each state
%%   variable in the model.
%%
%%   INIT_PARAMS is a record (structure array) that defines the value of each
%%   independent and dependent model parameter.
%%
%%   EVENTS is an initialised record (structure array) for recording any events
%%   of interest that occur during a single simulation.
function events = init_events(init_state, init_params)
    events = struct( ...
    'startPh1', 0, ... % set to 1 when the Contain phase starts
    'endPh1', 0, ... % set to 1 when the Contain phase ends
    'stockpileExpired', 0, ... % set to 1 when the stockpile expires
    'resCasesAtExpiry', 0, ... % the number of resistant cases when the stockpile expired
    'resPropnAtExpiry', 0, ... % the proportion of resistant cases when the stockpile expired
    'useSyndromic', 0, ... % set to 1 when switching to syndromic diagnosis
    'time_diagCapExceeded', -1, ... % set to the time when diagnostic capacity was exceeded
    'time_startPh1', -1, ... % set to the time when the Contain phase starts
    'time_endPh1', -1, ... % set to the time when the Contain phase ends
    'time_stockpileExpired', -1 ... % set to the time when the stockpile expires
    );
end

%% PARAMETER_SWEEP(sweep_name)
%
% Determines which parameter values to loop over when collecting results.
%
% Sweep name must be one of the following values:
%
% - 'default': the default parameter sweep (sweep_name can be omitted).
%
%See also collect_results

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function vals = parameter_sweep(sweep_name)
    if nargin < 1
        sweep_name = 'default';
    end

    switch sweep_name
        case 'default'
            vals = struct(...
                'R0', [1 2 3], ...
                'Severity', [4], ...
                'Pm', [1], ...
                'Qeff', [0.5], ...
                'Meff', [0 0.8], ...
                'rho', [0 0.8]);
        otherwise
            error('Unknown parameter sweep name: "%s"', sweep_name)
    end

    %% NOTE: this must match the array defined in "experiments/template.in".
    vals.R0_values = 2.53 * [1.0 0.75 2/3];

    vals.exp_fmt = 'scen_R0%d_sev%d_pm%d_qeff%.1f_meff%.1f_rho%.1f';

end

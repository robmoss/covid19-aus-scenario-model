# COVID-19 Australian scenario model

See the [technical appendix](doc/appendix.pdf) for a model description.

## License

Copyright 2020, Robert Moss.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

The documentation is licensed under a Creative Commons Attribution-ShareAlike
4.0 International License: https://creativecommons.org/licenses/by-sa/4.0/

## Installation

Ensure that the [LHS framework](https://bitbucket.org/robmoss/lhs_framework/)
has been installed and added to the MATLAB search path:

    addpath(genpath(fullfile(pwd, '..', 'lhs_framework')));

Ensure that the scenario model has been added to the MATLAB search path:

    addpath(genpath(pwd));

## Running a scenario simulation

You can then run the simulations for any of the provided pandemic scenarios.
For example, to run the unmitigated epidemic for `R0 = 2.53`:

    scen_R01_sev4_pm1_qeff05_meff0_rho0();

This will produce an output file in `./experiments/data` of the form:

    scen_R01_sev4_pm1_qeff05_meff0_rho0_YYYY-MM-DD_hh-mm.mat

You can then run the simulations for the same R0, but with case isolation and
self-quarantine measures in place:

    scen_R01_sev4_pm1_qeff05_meff08_rho08();

## Running all of the scenario simulations

The simulations can make use of multiple cores, but the memory requirements
may become prohibitive. You can run all of the scenarios using N parallel
workers by running:

    run_all(N);

The results for each scenario will be stored as a separate `.mat` file in
`experiments/data`.

## Collecting the simulation results

Once you have run **all** of the scenario simulations, you can collect the
outputs from each of these scenarios into a single CSV file:

    collect_results();

This will produce the output file `results.csv`.

You can also collect the daily incidence of cases that require an ICU bed:

    collect_icu_reqs();

This will produce the output file `icu_demand.csv`.

## Plotting the simulation results

A plotting script is provided in the `figures` directory, which will produce
all of the figures from the accompanying manuscript:

    cd figures
    ./plot_figures.R

Note that this requires a working [R](https://www.r-project.org/) installation
with the following packages:

+ [ggplot2](https://ggplot2.tidyverse.org/)

+ [gridExtra](https://cran.r-project.org/web/packages/gridExtra/)

+ [reshape2](https://cran.r-project.org/web/packages/reshape2)

+ [scales](https://cran.r-project.org/web/packages/scales/)

+ [Cairo](https://cran.r-project.org/web/packages/Cairo/) for saving the plots
  as a PDF document. You can replace this with the plotting device of your
  choice, and modify the end of `plot_figures.R` accordingly.

%% Model Parameters
%%
%% This module contains the parameter values for the model. The parameters
%% are stored in a single structure, params, and can be accessed by name.
%%
%%   params = model_params();
%%   params.R0 = 2.38;
%%

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [params] = model_params(fixed_params)
    if nargin < 1
        % Default behaviour: do not override any parameters.
        fixed_params = struct();
    end

    %% Initialise the base parameter values
    %%
    %% The derived parameters are:
    %%   beta (from R0)
    %%   epsilon_i
    %%   psi_i
    %%

    %% Dummy parameter for LHS dummy runs.
    dummy = 0;

    %% Population demographics, assuming a total population of 24 million as
    %% per ABS population estimates 3238.0.55.001, June 2016:
    %%
    %%     Indigenous:     798,365  (we scale this down by 24.190907 / 24)
    %% Non-Indigenous:  23,392,542
    %%          Total:  24,190,907  (we round this to 24 million)
    %%
    %% https://www.abs.gov.au/ausstats/abs@.nsf/mf/3238.0.55.001
    %%
    %% We then separate these populations into the following age groups:
    %%
    %%   0-9, 10-18, 19-29, 30-39, 40-49, 50-59, 60-69, 70-79, 80+
    %%
    %% Note that we're no longer splitting out risk groups such as healthcare
    %% workers, pregnant women, or adults aged 19-65 with comorbidities.

    %% Total population size.
    N = 24e6;
    %% The population breakdown: Indigenous age groups first, from youngest to
    %% oldest, followed by non-Indigenous age groups.
    pi = [0.007690 0.006210 ...
           0.006310 0.003890 0.003640 0.002790 ...
           0.001620 0.000640 0.000220 ...
           0.123600 0.102770 ...
           0.152130 0.138140 0.131440 0.123530 ...
           0.099880 0.059310 0.036190]';
    if isfield(fixed_params, 'pi')
        pi = fixed_params.pi;
    end
    n = length(pi); % Number of groups.

    %% Introduce *preferential* mixing.
    %%
    %% Consider:
    %%     >> m = repmat(pi', n, 1);
    %%
    %% With homogeneous mixing, each row m(R, 1:end) is identical.
    %%
    %%     >> sum(m, 2)         %% Returns the same value for each row.
    %%     >> sum(m, 1)' ./ pi  %% Returns the same value for each column.
    %%
    %% So each ROW represents the distribution of contacts for ONE STRATUM.

    %% Kappa is the per-person contact rate matrix (i.e., people per year),
    %% assuming that the number of contacts is the same for each strata.
    if isfield(fixed_params, 'kappa')
        kappa = fixed_params.kappa
    else
        %% The *relative* mixing matrix, characterising homogeneous mixing.
        m = repmat(pi', n, 1);

        %% Increase the proportion of mixing between indigenous strata.
        ix_mix = [1 2 3 4 5 6 7 8 9];
        %% Assume 80% of mixing occurs between indigenous individuals.
        propn_mix = 0.8;
        for ix = ix_mix
            m(ix, ix_mix) = propn_mix / length(ix_mix);
            ix_rest = ~ ismember(1:n, ix_mix);
            m(ix, ix_rest) = m(ix, ix_rest) * ...
                             (1 - propn_mix) / sum(m(ix, ix_rest));
        end

        %% Construct the *absolute* mixing matrix, which characterises the
        %% contact rates between each pair of strata.
        num_contacts = 20;
        kappa = num_contacts * m * 365;
    end

    %% Duration of contact tracing, following ascertainment of source case.
    %% Follow contacts for 14 days;
    delta = 365 / 14 * ones(n, 1);

    %% Transmission Probability Matrix (tpm) (matrix): given contact between
    %% S and I, probability of transmission.
    %% NOTE: take care, must consider which way around this is.
    tpm = ones(n);

    %% Incubation period of 5.2 days.
    incubation_period = 5.2;
    incubation_period_E2 = 2.0;
    incubation_period_E1 = incubation_period - incubation_period_E2;
    sigma_1 = 365 / incubation_period_E1 * ones(n, 1);
    sigma_2 = 365 / incubation_period_E2 * ones(n, 1);

    %% Infectious period of 7.68 days.
    infectious_period = 1 / 0.1302;
    infectious_period_I1 = 2.0;
    infectious_period_I2 = infectious_period - infectious_period_I1;
    gamma_1 = 365 / infectious_period_I1 * ones(n, 1);
    gamma_2 = 365 / infectious_period_I2 * ones(n, 1);
    %% Assume that self-quarantined cases will be identified in only 1 day,
    %% rather than in 2 days.
    infectious_period_I1q = 1.0;
    infectious_period_I2q = infectious_period - infectious_period_I1q;
    gamma_1q = 365 / infectious_period_I1q * ones(n, 1);
    gamma_2q = 365 / infectious_period_I2q * ones(n, 1);

    %% Basic reproduction number (baseline, does not account for mixing).
    R0 = 2.68;

    %% Intervention effect.
    %% All presenting cases are ascertained and can be effectively isolated.
    PrAscertain = 1.0;
    %% Assume that isolation reduces infectiousness by 80%.
    ImpactOfAscertain = 0.8;

    %% The proportion of contacts who will self-quarantine.
    rho = 0.5;
    %% Assume that self-quarantine reduces infectiousness by 50%.
    ImpactOfQuarantine = 0.5;

    %% Constant importation rate of 10 exposures per week.
    LambdaImport = 365 * 10 / 7;

    %% NOTE: this is the initial number of exposures.
    initial_exposures = 20;
    E1_init = initial_exposures * incubation_period_E1 / incubation_period;
    E2_init = initial_exposures * incubation_period_E2 / incubation_period;

    %% Proportion of infections that are severe (all of which present).
    eta = 0.1 * ones(n, 1);

    %% The relative risk of hospitalisation.
    %% NOTE: if you change this, you must also update the following parameters
    %% in experiments/model_of_care.m:
    %%   - ward_to_ICU
    %%   - ICU_to_death
    %%   - noICU_to_death
    %%
    %% Here are age-specific hospitalisation proportions, as provided by
    %% James Wood (2020-04-01) for ages 0-9, 10-18, 19-29, 30-39, 40-49,
    %% 50-59, 60-69, 70-79, 80+:
    age_hosp = [0.00062 0.00062 0.00775 0.02900 0.05106 ...
                0.09895 0.15493 0.35762 0.65937];
    %% The proportion of those hospitalised that require ICU.
    age_icu = 0.29355 * ones(9, 1);

    %% Record P(ICU | Hospitalisation) for each strata.
    if isfield(fixed_params, 'propn_icu')
        propn_icu = fixed_params.propn_icu
    else
        propn_icu = ones(n, 1);
        propn_icu(1:9) = age_icu;
        propn_icu(10:18) = age_icu;
    end

    %% Scale this so that eta = 10% -> use the proportions above.
    if isfield(fixed_params, 'eta_relscale')
        eta_relscale = fixed_params.eta_relscale
    else
        eta_relscale = ones(n, 1);
        eta_relscale(1:9) = 10 * age_hosp;
        eta_relscale(10:18) = 10 * age_hosp;
    end

    %% Simulation time parameters
    time_start = 0;
    time_end = 2; % 2 years
    %% NOTE: resolution of 16 steps per day.
    %% In previous (pandemic flu) work, a coarser resolution caused issues.
    time_entries = time_end * 365 * 16;

    %% Solve the model using Euler, suitable for stochastic models.
    solve_discretely = 1;

    %% Export the parameters in a single structure
    params = struct(...
        'N', N, ...
        'pi', pi, ...
        'kappa', kappa, ...
        'delta', delta, ...
        'tpm', tpm, ...
        'sigma_1', sigma_1, ...
        'sigma_2', sigma_2, ...
        'gamma_1', gamma_1, ...
        'gamma_2', gamma_2, ...
        'gamma_1q', gamma_1q, ...
        'gamma_2q', gamma_2q, ...
        'R0', R0, ...
        'PrAscertain', PrAscertain, ...
        'ImpactOfAscertain', ImpactOfAscertain, ...
        'rho', rho, ...
        'ImpactOfQuarantine', ImpactOfQuarantine, ...
        'LambdaImport', LambdaImport, ...
        'E1_init', E1_init, ...
        'E2_init', E2_init, ...
        'eta', eta, ...
        'eta_relscale', eta_relscale, ...
        'propn_icu', propn_icu, ...
        'time_start', time_start, ...
        'time_end', time_end, ...
        'time_entries', time_entries, ...
        'solve_discretely', solve_discretely, ...
        'dummy',dummy);

    % Add all parameters defined in the provided struct, if any, replacing any
    % pre-existing values defined above.
    overrides = fieldnames(fixed_params);
    for i = 1:length(overrides)
        field_name = overrides{i};
        replacement = fixed_params.(field_name);
        params.(field_name) = replacement;
    end

    %
    % Sanity checks: ensure parameter dimensions are as expected.
    %
    params_okay = true();

    % Ensure the strata vector is a column vector (thanks to Eamon Conway).
    params.pi = params.pi(:);

    % Ensure that the population is conserved across the strata.
    if abs(sum(params.pi) - 1.0) > 1e-10
        fprintf('Population strata do not sum to 1.0\n');
        params_okay = false();
    end

    % Ensure that other parameters have dimensions consistent with this number
    % of strata.
    strata_count = length(params.pi);
    strata_size = size(params.pi);
    square_size = [strata_count strata_count];

    % The model parameters whose size should match the population strata.
    same_size = {'delta', 'sigma_1', 'sigma_2', ...
                 'gamma_1', 'gamma_2', 'gamma_1q', 'gamma_2q', ...
                 'eta', 'eta_relscale', 'propn_icu'};
    % The model parameters that should be square matrices whose size should
    % match the population strata.
    square_mat = {'kappa', 'tpm'};

    for i = 1:length(same_size)
        field_name = same_size{i};
        if ~ isfield(params, field_name)
            params_okay = false();
            fprintf('Missing parameter "%s"\n', field_name);
        end
        actual_size = size(getfield(params, field_name));
        if ~ isequal(actual_size, strata_size)
            params_okay = false();
            fprintf('Parameter "%s" has incorrect size\n', field_name);
        end
    end

    for i = 1:length(square_mat)
        field_name = square_mat{i};
        if ~ isfield(params, field_name)
            params_okay = false();
            fprintf('Missing parameter "%s"\n', field_name);
        end
        actual_size = size(getfield(params, field_name));
        if ~ isequal(actual_size, square_size)
            params_okay = false();
            fprintf('Parameter "%s" has incorrect size\n', field_name);
        end
    end

    if ~ params_okay
        error('Parameter sizes not consistent with population strata')
    end
end

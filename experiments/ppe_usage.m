%% PPE_USAGE(moc_name, ppe_rec)
%
% Returns the background and per-patient PPE rates in each healthcare setting,
% for the given model of care and PPE recommendations.
%

% Copyright 2016, 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [usage] = ppe_usage(moc_name, ppe_rec)
  switch ppe_rec

    %% Define the PPE usage as per the default recommendations.
    case 'moderate'
      ppe_bg_icu = NaN;
      if strcmp(moc_name, 'cohort')
        ppe_bg_ward = NaN;
      else
        ppe_bg_ward = NaN;
      end
      ppe_bg_ed = NaN;  %% NOTE: not affected by cohorting.
      ppe_bg_gp = NaN;
      if strcmp(moc_name, 'clinics')
          ppe_bg_clinic = NaN;
      else
          ppe_bg_clinic = 0;
      end
      ppe_case_icu = NaN;
      ppe_case_ward = NaN;
      ppe_case_ed = NaN;
      ppe_case_gp = NaN;
      if strcmp(moc_name, 'clinics')
          ppe_case_clinic = NaN;
      else
          %% NOTE: the phone/online model uses the clinic setting to track
          %% consultations, but uses no PPE in doing so.
          ppe_case_clinic = 0;
      end

      %% P2 mask consumption (per-patient only, no overheads).
      p2_case_icu = NaN;
      p2_case_ward = NaN;
      p2_case_ed = NaN;
      p2_case_ed_severe = NaN;
      p2_case_gp = NaN;
      p2_case_gp_severe = NaN;
      if strcmp(moc_name, 'clinics')
          p2_case_clinic = NaN;
          p2_case_clinic_severe = NaN;
      else
          p2_case_clinic = NaN;
          p2_case_clinic_severe = NaN;
      end

    otherwise
      error('Invalid PPE recommendations: "%s".', ppe_rec);
    end

    %% Return the PPE usage parameters.
    usage = struct(...
      'recommendation', ppe_rec, ...
      'daily_bg_ICU', ppe_bg_icu, ...
      'daily_bg_Ward', ppe_bg_ward, ...
      'daily_bg_ED', ppe_bg_ed, ...
      'daily_bg_GP', ppe_bg_gp, ...
      'daily_bg_Clinic', ppe_bg_clinic, ...
      'daily_case_ICU', ppe_case_icu, ...
      'daily_case_Ward', ppe_case_ward, ...
      'daily_case_ED', ppe_case_ed, ...
      'daily_case_GP', ppe_case_gp, ...
      'daily_case_Clinic', ppe_case_clinic, ...
      'p2_case_icu', p2_case_icu, ...
      'p2_case_ward', p2_case_ward, ...
      'p2_case_ed', p2_case_ed, ...
      'p2_case_ed_severe', p2_case_ed_severe, ...
      'p2_case_gp', p2_case_gp, ...
      'p2_case_gp_severe', p2_case_gp_severe, ...
      'p2_case_clinic', p2_case_clinic, ...
      'p2_case_clinic_severe', p2_case_clinic_severe);
end

%% Default Parameter Probability Distributions
%
% Provides default parameter distributions for each model parameter, to be used
% for Latin hypercube sampling.
%

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [param_dists] = default_dists()

    % load the default fixed, initial values of the model parameters
    % for use in building the default distributions.
    params = model_params();

    % the default (and hopefully sensible) parameter distributions
    % make sure this respects that all interventions are OFF by default

    param_dists = struct( ...
        'R0', dist_flat(1.2,1.5), ...
        'eta', dist_logflat(-3,-1));
end

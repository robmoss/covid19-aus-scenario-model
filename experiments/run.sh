#!/bin/sh
##
## Run an LHS experiment.
##
## Copyright 2016 Robert Moss.
## SPDX-License-Identifier: GPL-3.0-or-later

## The default number of parallel workers.
N=16
## The MATLAB command-line options.
MATLAB_ARGS="-nodisplay -nosplash -nodesktop"

## Check whether to update the number of parallel workers.
if [ "$#" -ge 2 ] && [ "$1" = "-n" ]; then
    ## Ensure the provided value is a positive integer.
    if [ "$2" -eq "$2" ] 2> /dev/null && [ "$2" -gt 0 ]; then
        N="$2"
        shift 2
    else
        echo "ERROR: -n requires a positive integer"
        exit 2
    fi
fi

## Display a usage message and quit if the arguments are not valid.
if [ "$#" -lt 1 ]; then
    echo "USAGE: $(basename $0) [-n NUM_WORKERS] scenario_file.m [...]"
    echo ""
    echo "Options:"
    echo "    -n NUM_WORKERS    Set the number of parallel workers"
    echo "                      Default: ${N}"
    echo ""
    exit 2
fi

FUNCTIONS=""
for ARG in "$@"; do
    SCRIPT=$(basename -- "${ARG}")
    if [ ! -r "${SCRIPT}" ]; then
        echo "ERROR: can not read file '${SCRIPT}'"
        exit 2
    fi
    FUNCTION=$(basename -- "${ARG}" .m)
    if [ -z "${FUNCTIONS}" ]; then
        FUNCTIONS="${FUNCTION}"
    else
        FUNCTIONS="${FUNCTIONS}; ${FUNCTION}"
    fi
done

## Ensure the working directory is the directory that contains this script.
OLD_WD=$(pwd)
cd "$(dirname "$0")"

## Run the LHS experiment and terminate the MATLAB session.
matlab ${MATLAB_ARGS} -r "parpool(${N}); ${FUNCTIONS}; exit"

## Restore the previous working directory (probably unnecessary).
cd "${OLD_WD}"

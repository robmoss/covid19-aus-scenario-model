%%RUN_TESTS()
%
%Run a suite of test cases and return whether all tests succeeded.

% Copyright 2020 Robert Moss.
% SPDX-License-Identifier: GPL-3.0-or-later
function [success] = run_tests()
    test_suite = {...
                     @test_strata_dims ...
                 };

    success = true();
    num_pass = 0;
    num_fail = 0;
    num_tests = length(test_suite);

    fprintf('Running %d tests ...\n', num_tests);
    for i = 1:num_tests
        test_func = test_suite{i};
        test_name = func2str(test_func);
        test_result = test_func();
        if test_result
            num_pass = num_pass + 1;
            fprintf('    PASS: %s\n', test_name);
        else
            num_fail = num_fail + 1;
            fprintf('    FAIL: %s\n', test_name);
        end
        success = success && test_result;
    end

    if num_pass == num_tests
        fprintf('Ran %d tests: all passed\n', num_tests);
    else
        fprintf('Ran %d tests: %d passed and %d failed\n', ...
                num_tests, num_pass, num_fail);
    end
end

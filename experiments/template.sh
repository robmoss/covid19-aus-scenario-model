#!/bin/sh
#
# Copyright 2020 Robert Moss.
# SPDX-License-Identifier: GPL-3.0-or-later

# R0 values here are indices into the R0 vector returned by parameter_sweep().
VALS_R0="1 2 3"
# NOTE: for now, only consider the high-severity scenario.
VALS_Sev="4"
VALS_PM="1"
VALS_Qeff="0.5"
VALS_Meff="0 0.8"
VALS_rho="0 0.8"

IN=template.in
RUN_ALL="run_all.m"

CMDS=""

for R0 in ${VALS_R0}; do
    for Sev in ${VALS_Sev}; do
        for PM in ${VALS_PM}; do
            for Qeff in ${VALS_Qeff}; do
                for Meff in ${VALS_Meff}; do
                    for rho in ${VALS_rho}; do
                        if [ "${rho}" != "${Meff}" ]; then
                            continue
                        fi
                        if [ "${R0}" -gt "1" ] && [ "${rho}" = "0" ]; then
                            continue
                        fi
                        # NOTE: this will contain periods, which are not
                        # valid characters for a Matlab function.
                        RAW_BASE="scen_R0${R0}_sev${Sev}_pm${PM}_qeff${Qeff}_meff${Meff}_rho${rho}"
                        BASE=$(echo "${RAW_BASE}" | sed 's/\.//g')
                        OUT="${BASE}.m"
                        (sed "s/RR/${R0}/" < "${IN}" |
                             sed "s/SS/${Sev}/" |
                             sed "s/PM/${PM}/" |
                             sed "s/QE/${Qeff}/" |
                             sed "s/ME/${Meff}/" |
                             sed "s/RHO/${rho}/" |
                             sed "s/${RAW_BASE}/${BASE}/"> "${OUT}")
                        if [ -z "${CMDS}" ]; then
                            CMDS="    ${BASE}();"
                        else
                            CMDS="${CMDS}\n    ${BASE}();"
                        fi
                    done
                done
            done
        done
    done
done

{
    echo "%% RUN_ALL(workers)"
    echo "%"
    echo "% Runs all of the scenario simulations, using the specified number"
    echo "% of parallel workers (default: do not use any parallel workers)."
    echo ""
    echo "% Copyright 2020 Robert Moss."
    echo "% SPDX-License-Identifier: GPL-3.0-or-later"
    echo "function run_all(workers)"
    echo "    if nargin > 0"
    echo "        parpool(workers)"
    echo "    end"
    echo "${CMDS}"
    echo "end"
} > "${RUN_ALL}"
